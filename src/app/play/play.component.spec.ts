import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayComponent } from './play.component';

describe('PlayComponent', () => {
  let component: PlayComponent;
  let fixture: ComponentFixture<PlayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#hasSnake', () => {
  it ('should be return true', () => {
    component.snake = [[0, 0]];
    expect(component.hasSnake(0, 0)).toBe(true)
  })});

  it ('should be return false', () => {
    component.snake = [[0, 1]];
    expect(component.hasSnake(0, 0)).toBe(false)
  })
});

