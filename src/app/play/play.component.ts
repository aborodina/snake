import {Component, OnInit} from '@angular/core';

const FIELD_SIZE = 10;

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss']
})
export class PlayComponent implements OnInit {

  field = new Array(FIELD_SIZE).fill(new Array(FIELD_SIZE).fill(''));
  snake: number[][];

  constructor() {
    this.startGame();
  }

  ngOnInit() {
  }

  startGame() {
    this.snake = [[0, 0], [0, 1], [0, 2]];
  }

  hasSnake(i, j): boolean {
    return !!this.snake.find((value: number[]) => value[0] === i && value[1] === j);
  }
}
